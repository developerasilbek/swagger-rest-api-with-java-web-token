package com.example.pathient.Entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String illness;
    private Long pinfl;

    public Patient(String name, String illness, Long pinfl) {
        this.name = name;
        this.illness = illness;
        this.pinfl = pinfl;
    }
}
