package com.example.pathient.components;

import com.example.pathient.Entity.Huquq;
import com.example.pathient.Entity.Role;
import com.example.pathient.Entity.User;
import com.example.pathient.enums.HuquqName;
import com.example.pathient.enums.RoleName;
import com.example.pathient.repository.HuquqRepository;
import com.example.pathient.repository.RoleRepository;
import com.example.pathient.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    private String mode;

    @Autowired
    UserRepository userRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    HuquqRepository huquqRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (mode.equals("always")){
            Set<Role> rollar = new HashSet<>();
            Set<Huquq> huquqlar = new HashSet<>();
            RoleName[] values = RoleName.values();
            for (RoleName value : values) {
                rollar.add(roleRepo.save(new Role(value)));
            }

            HuquqName[] values1 = HuquqName.values();
            for (HuquqName huquqName : values1) {
                huquqlar.add(huquqRepo.save(new Huquq(huquqName)));
            }
            User projectOwner = new User();
            projectOwner.setLogin("admin");
            projectOwner.setParoli(passwordEncoder.encode("admin"));
            projectOwner.setHuquqlari(huquqlar);
            projectOwner.setRoles(rollar);
            projectOwner.setPhoneNumber("+998930642224");
            projectOwner.setEmail("AsilbekSanaqulov@gmail.com");
            projectOwner.setFio("Corporation");
            userRepo.save(projectOwner);
        }
    }
}
