package com.example.pathient.controller;

import com.example.pathient.payload.ApiResponse;
import com.example.pathient.payload.SignIn;
import com.example.pathient.security.JwtTokenProvider;
import com.example.pathient.service.AuthService;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("api/auth")
public class AuthController {


    @Autowired
    AuthService authService;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SignIn signIn) throws Exception {
        ApiResponse response = authService.login(signIn);
        return ResponseEntity.ok(response);
    }




}
