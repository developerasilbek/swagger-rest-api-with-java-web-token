package com.example.pathient.controller;

import com.example.pathient.Entity.Patient;
import com.example.pathient.payload.PatientDto;
import com.example.pathient.service.PatientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/patient")
public class PatientController {

    @Autowired
    PatientService patientService;

    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody PatientDto patientDto) {
        com.example.pathient.payload.ApiResponse save = patientService.save(patientDto);
        return ResponseEntity.ok(save);
    }

    @Operation(summary = "Barcha bemorlar ro'yxati")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Topilgan bemorlar",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Patient.class)) }),
            @ApiResponse(responseCode = "404", description = "Topilmadi",
                    content = @Content) })
    @GetMapping("/all")
    public HttpEntity<?> all() {
        List<Patient> all =patientService.all();
        return ResponseEntity.ok(all);
    }

    @Operation(summary = "Bemor")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Topilgan bemor",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Patient.class)) }),
            @ApiResponse(responseCode = "404", description = "Topilmadi",
                    content = @Content) })
        @GetMapping("/{pinfl}")
    public Patient getByPinfl(@PathVariable Long pinfl) {
            Patient byPinfl = patientService.getByPinfl(pinfl);
        return byPinfl;
    }

    @DeleteMapping("/{pinfl}")
    public HttpEntity<?> deleteById(@PathVariable Long pinfl) {
        com.example.pathient.payload.ApiResponse apiResponse = patientService.deleteBypinfl(pinfl);
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping("/{id}")
    public HttpEntity<?> editById(@PathVariable Integer id,
                                  @RequestBody PatientDto patientDto) {
        com.example.pathient.payload.ApiResponse apiResponse = patientService.updateById(id, patientDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/getByPage")
    public HttpEntity<?> getByPage(@RequestParam int page, @RequestParam int size){
        com.example.pathient.payload.ApiResponse byPageable = patientService.getByPageable(page, size);
        return ResponseEntity.status(byPageable.isSuccess()?201:409).body(byPageable);
    }
}
