package com.example.pathient.enums;

public enum HuquqName {
    ADD_PATIENT,
    DELETE_PATIENT,
    EDIT_PATIENT,
    GET_PATIENT
}
