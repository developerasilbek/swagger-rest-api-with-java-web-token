package com.example.pathient.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_DIRECTOR
}
