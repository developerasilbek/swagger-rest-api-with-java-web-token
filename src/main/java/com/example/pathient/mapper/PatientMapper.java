package com.example.pathient.mapper;

import com.example.pathient.Entity.Patient;
import com.example.pathient.payload.PatientDto;

public class PatientMapper {

    public static Patient patientFromDto(PatientDto dto) {
        Patient patient = new Patient();
        if (dto.getId() != null) {
            patient.setId(dto.getId());
        }
        patient.setName(dto.getName());
        patient.setIllness(dto.getIllness());
        patient.setPinfl(dto.getPinfl());
        return patient;
    }

    public static PatientDto dtoFromPatient(Patient patient) {
        return new PatientDto(patient.getId(), patient.getName(), patient.getIllness(), patient.getPinfl());
    }
}
