package com.example.pathient.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PatientDto {
    private Integer id;
    private String name;
    private String illness;

    private Long pinfl;

    public PatientDto(String name, String illness, Long pinfl) {
        this.name = name;
        this.illness = illness;
        this.pinfl = pinfl;
    }
}
