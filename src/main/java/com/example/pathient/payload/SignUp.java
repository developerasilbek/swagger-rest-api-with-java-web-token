package com.example.pathient.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SignUp {
    @NotNull
    private String fio;

    @NotNull
    @Pattern(regexp = "(\\+998)[0-9]{9}", message = "Tel nomerni togri yozing")
    private String phoneNumber;

    @NotNull
    @Email
    private String email;

    @NotNull
    private String login;

    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+!])(?=\\S+$).{8,}", message = "Kamida 8ta belgi, bitta bosh harf, bitta kichik harf, son va belgilar ishtirok etishi kerak")
    @NotNull
    private String password;

    @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+!])(?=\\S+$).{8,}", message = "Kamida 8ta belgi, bitta bosh harf, bitta kichik harf, son va belgilar ishtirok etishi kerak")
    @NotNull
    private String rePassword;
}
