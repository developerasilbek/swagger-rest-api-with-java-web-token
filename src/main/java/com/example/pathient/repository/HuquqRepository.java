package com.example.pathient.repository;

import com.example.pathient.Entity.Huquq;
import com.example.pathient.enums.HuquqName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HuquqRepository extends JpaRepository<Huquq,Long> {
    Huquq findByHuquqName(HuquqName huquqName);
}
