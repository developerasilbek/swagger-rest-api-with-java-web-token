package com.example.pathient.repository;

import com.example.pathient.Entity.Patient;
import com.example.pathient.payload.PatientDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    Optional<Patient> findByName(String name);
    Optional<Patient> findByPinfl(Long pinfl);
    Patient findByIllness(String illness);
    Optional<Patient> deleteByPinfl(Long pinfl);
}
