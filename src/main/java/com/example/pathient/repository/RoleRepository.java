package com.example.pathient.repository;

import com.example.pathient.Entity.Role;
import com.example.pathient.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByRoleName(RoleName roleName);

}
