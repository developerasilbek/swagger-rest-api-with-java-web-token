package com.example.pathient.security;

import com.example.pathient.Entity.User;
import com.example.pathient.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    JwtTokenProvider provider;

    @Autowired
    UserRepository userRepository;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        User user = getUserFromToken(httpServletRequest);
        if (user != null) {
            if (user.isEnable() &&
                    user.isCredentialsNonExpired()
                    && user.isAccountNonLocked()
                    && user.isAccountNonExpired()) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities()
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }


    public String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        return token != null ? token.substring(7) : null;
    }

    public User getUserFromToken(HttpServletRequest request) {
        String token = getTokenFromRequest(request);
        if (token != null && provider.validateToken(token)) {
            Long userId = provider.getSubjectFromToken(token);

            return userRepository.findById(userId).orElse(null);
        }
        return null;
    }
}
