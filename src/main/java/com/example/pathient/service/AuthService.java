package com.example.pathient.service;

import com.example.pathient.Entity.User;
import com.example.pathient.payload.ApiResponse;
import com.example.pathient.payload.ResToken;
import com.example.pathient.payload.SignIn;
import com.example.pathient.repository.HuquqRepository;
import com.example.pathient.repository.RoleRepository;
import com.example.pathient.repository.UserRepository;
import com.example.pathient.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    HuquqRepository huquqRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager manager;

    @Autowired
    JwtTokenProvider provider;

    public ApiResponse login(SignIn signIn) {
        Authentication authenticate = manager.authenticate(new UsernamePasswordAuthenticationToken(signIn.getUsername(), signIn.getPassword()));
        Optional<User> byLogin = userRepository.findByLogin(signIn.getUsername());
        if (byLogin.isPresent() && byLogin.get().isEnable()) {
            String token = provider.generateToken((User) authenticate.getPrincipal());
            return new ApiResponse("ok", true, new ResToken(token));
        }
        return new ApiResponse("ok", false);

    }

}
