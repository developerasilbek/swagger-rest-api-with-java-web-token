package com.example.pathient.service;

import com.example.pathient.Entity.Patient;
import com.example.pathient.mapper.PatientMapper;
import com.example.pathient.payload.ApiResponse;
import com.example.pathient.payload.PatientDto;
import com.example.pathient.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {

    }


    public ApiResponse save(PatientDto patientDto) {
        patientRepository.save(PatientMapper.patientFromDto(patientDto));
        return new ApiResponse("Saved", true);
    }

    public List<Patient> all() {
        return patientRepository.findAll();
    }

    public Patient getByPinfl(Long pinfl) {
        Optional<Patient> byPinfl = patientRepository.findByPinfl(pinfl);
        return byPinfl.orElse(null);
    }

    public ApiResponse deleteBypinfl(Long pinfl) {
        Optional<Patient> byId = patientRepository.findByPinfl(pinfl);
        if (byId.isPresent()) {
            patientRepository.deleteByPinfl(pinfl);
            return new ApiResponse("Successfully deleted", true);
        }
        return new ApiResponse("not exsist", false);
    }

    public ApiResponse updateById(Integer id, PatientDto patientDto) {
        Optional<Patient> byId = patientRepository.findById(id);
        Patient patient = new Patient();
        if (byId.isPresent()) {
            patient.setId(id);
            patient.setName(patientDto.getName());
            patient.setIllness(patientDto.getIllness());
            patient.setPinfl(patientDto.getPinfl());
            patientRepository.saveAndFlush(patient);
            return new ApiResponse("Successfully edited", true, patient);
        }
        return new ApiResponse("not exsist", false);
    }

    public ApiResponse getByPageable(int page, int size) {
        Page<Patient> all = patientRepository.findAll(PageRequest.of(page - 1, size, Sort.by("id").ascending()));
        List<PatientDto> patientDtoList = new ArrayList<>();
        for (Patient patient : all) {
            PatientDto patientDto = new PatientDto();
            patientDto.setId(patient.getId());
            patientDto.setName(patient.getName());
            patientDto.setIllness(patient.getIllness());
            patientDto.setPinfl(patient.getPinfl());

            patientDtoList.add(patientDto);
        }
        return new ApiResponse("Patient By Page", true, patientDtoList, all.getTotalElements());
    }

}
