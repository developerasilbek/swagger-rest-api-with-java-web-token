package com.example.pathient.repository;

import com.example.pathient.Entity.Patient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PatientRepositoryTest {

    @Autowired
    private PatientRepository patientRepository;


    @Test
    @Rollback(value = false)
    @Order(1)
    public void savePatientTest(){
        Patient patient = Patient.builder()
                .name("aaa")
                .illness("Tish ogrigi")
                .build();

        patientRepository.save(patient);
        Assertions.assertThat(patient.getId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    public void getPatientTest(){
        Patient patient = patientRepository.findById(1).get();
        Assertions.assertThat(patient.getId()).isEqualTo(1);

    }

    @Test
    @Order(3)
    public void getListOfPatientsTest(){
        List<Patient> patient = patientRepository.findAll();
        Assertions.assertThat(patient.size()).isGreaterThan(0);
    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updatePatientTest(){
        Patient patient = patientRepository.findById(1).get();
        patient.setName("bbb");
        Patient patientUpdated = patientRepository.save(patient);
        Assertions.assertThat(patientUpdated.getName()).isEqualTo("bbb");
    }

    @Test
    @Order(5)
    public void deletePatientTest(){
        Patient patient = patientRepository.findById(1).get();
        patientRepository.delete(patient);

        Patient patient1 = null;
        Optional<Patient> optionalPatient = patientRepository.findByName("bbbb");

        if (optionalPatient.isPresent()){
            patient1 = optionalPatient.get();
        }

        Assertions.assertThat(patient1).isNull();
    }

}