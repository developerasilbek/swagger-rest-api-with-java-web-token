package com.example.pathient.service;


import com.example.pathient.Entity.Patient;
import com.example.pathient.repository.PatientRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PatientServiceTest {

    @Autowired
    private PatientRepository patientRepository;


    @Test
    @Rollback()
    @Order(1)
    public void testCreateProduct() {
        Patient patient = patientRepository.save(new Patient("cccc", "dddd",1111L));
        assertThat(patient.getId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    public void testFindProductById() {
        Patient patient = patientRepository.findById(3).get();
        Assertions.assertThat(patient.getId()).isEqualTo(3);
    }

    @Test
    @Order(3)
    public void testListProducts() {
        List<Patient> products = patientRepository.findAll();
        assertThat(products).size().isGreaterThan(0);
    }

    @Test
    @Rollback(false)
    @Order(4)
    public void testUpdateProduct() {
        Patient patient = patientRepository.findById(3).get();
        patient.setName("qqqq");

        patientRepository.save(patient);

        Patient updatedPatient = patientRepository.findById(3).get();

        assertThat(updatedPatient.getName()).isEqualTo("qqqq");
    }

    @Test
    @Order(5)
    public void deletePatientTest(){
        Patient patient = patientRepository.findById(3).get();
        patientRepository.delete(patient);

        Patient patient1 = null;
        Optional<Patient> optionalPatient = patientRepository.findByName("cccc");

        if (optionalPatient.isPresent()){
            patient1 = optionalPatient.get();
        }

        Assertions.assertThat(patient1).isNull();
    }
}